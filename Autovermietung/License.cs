using System;

namespace Autovermietung
{
    [Serializable]
    public class License
    {
        public Customer Owner { get; }
        public bool IsValid { get; set; }

        public License(Customer owner, bool isValid = true)
        {
            Owner = owner;
            IsValid = isValid;
        }
        public void PrintInfo()
        {
            Console.Write($"License: \n" + 
                          $"\t\t Owner: {Owner.Name} ({Owner.Id})\n" +
                          $"\t\t Validity: {(IsValid ? "": "in")}valid\n");
        }
    }
}