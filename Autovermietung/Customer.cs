using System;

namespace Autovermietung
{
    [Serializable]
    public class Customer : Human
    {
        public License License { get; set; }

        public Customer(string name, Language nativeLanguage) : base(name, nativeLanguage)
        {
            
        }
        public override void PrintInfo()
        {
            Console.Write($"Customer:\t{Name}\n");
            base.PrintInfo();
            if (License != null)
                Console.Write($"\t\t License: {(License.IsValid ? "": "in")}valid\n");
            else 
                Console.Write($"\t\t License: none\n");
        }
    }
}