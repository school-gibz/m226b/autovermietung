﻿namespace Autovermietung
{
    public class Van : Vehicle
    {
        public bool HasSideDoor { get; }
        public int MaxWeight { get; }

        public Van (Manufacturer manufacturer, string model, double pricePerDay, int doors, int xWheelDrive, int maxWeight,
            bool isSelfDriving = false, bool hasTrailerHitch = false, Plate plate = null, bool hasSideDoor = true
            , Employee mechanic = null) :
            base (manufacturer, model, pricePerDay, doors, xWheelDrive, isSelfDriving, hasTrailerHitch, plate, mechanic)
        {
            HasSideDoor = hasSideDoor;
            MaxWeight = maxWeight;
        }
    }
}