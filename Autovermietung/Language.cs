﻿using System;

namespace Autovermietung
{
    [Serializable]
    public enum Language
    {
        De, En, It, Es, Fr, Tr
    }
}