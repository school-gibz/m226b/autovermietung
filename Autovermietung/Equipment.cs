using System;

namespace Autovermietung
{
    [Serializable]
    public class Equipment
    {
        public string Name { get; }
        public string Description { get; }

        public Equipment(string name, string description)
        {
            Name = name;
            Description = description;
        }

        public void PrintInfo()
        {                 
            Console.Write($"{Name}: {Description}");
            // TODO: Make description break at line
        }                  
    }
}