﻿using System;

namespace Autovermietung.Exception
{
    public class EmployeeNotMechanicException : System.Exception
    {
        public EmployeeNotMechanicException()
        :base("Employee is not a mechanic!")
        {
            
        }

        public EmployeeNotMechanicException(Employee employee)
        :base($"Employee {employee.Name} ({employee.Id}) is not a mechanic!")
        {
            
        }
    }
}