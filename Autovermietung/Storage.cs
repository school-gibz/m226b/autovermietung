﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Autovermietung
{
    [System.Serializable]
    class Storage {
        public static void WriteBinary(string fileName, object fileContent)
        {
            FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate);

            byte[] tmp = ObjectToByteArray(fileContent);
            fs.Write(tmp, 0, tmp.Length);

            fs.Close();
        }
        static byte[] ObjectToByteArray(object obj)
        {
            if (obj == null)
                return null;

            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, obj);

            return ms.ToArray();
        }

        public static void WriteJson(string fileName, object fileContent)
        {
            JsonSerializer serializer = new JsonSerializer();
            serializer.Converters.Add(new JavaScriptDateTimeConverter());
            serializer.NullValueHandling = NullValueHandling.Ignore;
            serializer.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            serializer.TypeNameHandling = TypeNameHandling.All;

            using (StreamWriter sw = new StreamWriter(fileName))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, fileContent);
            }
        }
    }
}