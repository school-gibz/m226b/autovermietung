using System;
using Autovermietung.Exception;

namespace Autovermietung
{
    [Serializable]
    public abstract class Vehicle
    {
        public Guid Id { get; }
        public Manufacturer Manufacturer { get; }
        public string Model { get; }
        public int Doors { get; }
        public int XWheelDrive { get; }
        public bool IsSelfDriving { get; }
        public bool HasTrailerHitch { get; set; }
        public Plate Plate { get; set; }
        private Employee _mechanicOnDamage;
        public Employee MechanicOnDamage
        {
            get
            {
                return _mechanicOnDamage;
            }
            set
            {
                if (value == null)
                {
                    _mechanicOnDamage = null;
                }
                else if (value.Department != WorkerType.Mechanic)
                {
                    throw new EmployeeNotMechanicException(value);
                }
                else
                {
                    _mechanicOnDamage = value;
                }
            }
        }

        public double PricePerDay { get; set; }

        protected Vehicle(Manufacturer manufacturer, string model, double pricePerDay, int doors, int xWheelDrive,
            bool isSelfDriving = false, bool hasTrailerHitch = false, Plate plate = null, Employee mechanic = null)
        {
            Id = Guid.NewGuid();
            Manufacturer = manufacturer;
            Model = model;
            PricePerDay = pricePerDay;
            Doors = doors;
            XWheelDrive = xWheelDrive;
            IsSelfDriving = isSelfDriving;
            HasTrailerHitch = hasTrailerHitch;
            Plate = plate;
            MechanicOnDamage = mechanic;
        }

        public virtual void PrintInfo()
        {
            Console.Write($"Vehicle: \t{Manufacturer.Name} {Model}\n" +
                          $"\t\t Driven axis:\t{XWheelDrive}\n" +
                          $"\t\t Price per day:\t{PricePerDay}\n" +
                          $"\t\t Doors: \t{Doors}\n" +
                          $"\t\t Plate: \t{(Plate == null ? "Not registered" : Plate.Number)}\n" +
                          $"\t\t Trailer hitch: {(HasTrailerHitch ? "Yes" : "No")}\n" +
                          $"\t\t Self driving: {(IsSelfDriving ? "Yes" : "No")}\n");
        }
    }
}