﻿using System;
using System.Collections.Generic;

namespace Autovermietung
{
    internal partial class Program
    {
        public void CreateContract()
        {
            
            Customer tmpCustomer = selectCustomer(); // TODO: Welcome message
            Employee tmpEmployee = selectEmployee();

            #region Select Vehicle

            List<Vehicle> selectedVehicles = new List<Vehicle>();

            do
            {

                Console.WriteLine("\nWhat object do you want to rent?");
                Console.WriteLine("C Car");
                Console.WriteLine("V Van");
                Console.WriteLine("B Bus");
                Console.WriteLine("T Truck\n");

                bool goBackVehicle = false;
                while (!goBackVehicle)
                    switch (Console.ReadLine().ToUpper())
                    {
                        case "C":
                            selectedVehicles.Add(cars[selectIdFromList(cars)]);
                            goBackVehicle = true;
                            break;
                        case "V":
                            selectedVehicles.Add(vans[selectIdFromList(vans)]);
                            goBackVehicle = true;
                            break;
                        case "B":
                            selectedVehicles.Add(buses[selectIdFromList(buses)]);
                            goBackVehicle = true;
                            break;
                        case "T":
                            selectedVehicles.Add(trucks[selectIdFromList(trucks)]);
                            goBackVehicle = true;
                            break;

                        default:
                            Console.WriteLine("Your selection is not valid, please try again!");
                            break;
                    }
            } while (YesOrNoQuestion("Do you want to add another vehicle?"));
            
            #endregion

            #region Select Duration

            int contractDuration = 0;
            while (contractDuration == 0)
            {
                Console.WriteLine("\nHow many days will the contract last");
                
                string rawInput = Console.ReadLine();
                if (!int.TryParse(rawInput, out _))
                    Console.WriteLine("Your input is not valid, please enter a number!");
                else if (Convert.ToInt32(rawInput) < 1)
                    Console.WriteLine("Your input is not valid, the contract needs to last at least 1 day!");
                else if (Convert.ToInt32(rawInput) > 90)
                    Console.WriteLine("Sorry, but a contract needs to last less than 90 days");
                else 
                    contractDuration = Convert.ToInt32(rawInput);
            }

            #endregion

            #region Show Cost
            
            double contractCost = 0;
            foreach (var v in selectedVehicles)
                contractCost += v.PricePerDay * contractDuration;  // account for the length of the contract

            Console.WriteLine($"\nTotal cost for {contractDuration} days: {contractCost} CHF");
            Console.WriteLine("Press any key to proceed to payment");
            Console.ReadKey();

            #endregion
            
            #region Payment System
            
            int selectedPaymentOption = -1;
            while (selectedPaymentOption == -1)
            {
                Console.WriteLine("\n\nSelect Payment Option:");

                string[] availablePaymentOptions = Enum.GetNames(typeof(PaymentOption));

                for (var i = 0; i < availablePaymentOptions.Length; i++)
                    Console.WriteLine($"{i} {availablePaymentOptions[i]}");

                string rawInput = Console.ReadLine();
                if (!int.TryParse(rawInput, out _))
                    Console.WriteLine("Your input is not valid, please enter a number!");
                else if (Convert.ToInt32(rawInput) > availablePaymentOptions.Length)
                    Console.WriteLine("Sorry, but that payment option does not exist");
                else 
                    selectedPaymentOption = Convert.ToInt32(rawInput);
            }

            #endregion

            #region Payment

            if (selectedPaymentOption == (int) PaymentOption.Cash)
            {
                // Customer paid with Cash
                Console.WriteLine("\nHow much money did the client hand you?");
                bool goBack = false;
                while (!goBack)
                {
                    string rawInput = Console.ReadLine();
                    if (!int.TryParse(rawInput, out _))
                        Console.WriteLine("Your input is not valid, please enter a number!");
                    else if (Convert.ToInt32(rawInput) < 1)
                        Console.WriteLine("Your input is not valid, please enter a positive number!");
                    else if (Convert.ToInt32(rawInput) < (contractCost))
                        Console.WriteLine($"This is not enough money to pay for the contract ({contractCost} CHF required), please try again!");
                    else
                    {
                        Console.WriteLine($"Payment successful, please return the {Convert.ToInt32(rawInput) - (contractCost)} CHF to the customer, thank you for your visit :)");
                        goBack = true;
                    }
                }
            }
            else
            {
                // Anything not cash
                Console.WriteLine($"\nPlease proceed with payment of {contractCost} CHF");
                Console.WriteLine("Press any key when payment is complete and verified");
                Console.ReadKey();
            }
            
            // add to database
            contracts.Add(new Contract(tmpCustomer, tmpEmployee, selectedVehicles, DateTime.Now, DateTime.Now.AddDays(contractDuration), contractCost));

            #endregion
        }
        
        private void Statistics()
        {
            int totalItems = 0;
            
            // Count together the amount of items from every object
            totalItems += cars.Count;
            totalItems += vans.Count;
            totalItems += buses.Count;
            totalItems += trucks.Count;
            totalItems += manufacturers.Count;
            totalItems += contracts.Count;
            totalItems += customers.Count;
            totalItems += employees.Count;
            // totalItems += equipment.Count;
            totalItems += licenses.Count;
            totalItems += plates.Count;
            
            // contract value
            double totalContractValue = 0;
            double contractValueToday = 0;
            double contractValueYesterday = 0;
            double contractValueWeek = 0;
            double contractValueMonth = 0;
            double contractValueYear = 0;

            foreach (Contract c in contracts)
            {
                totalContractValue = c.TotalCost;

                double hoursSince = (DateTime.Now - c.GetValidationDate()).TotalHours;
                if (hoursSince <= 24) contractValueToday += c.TotalCost;
                if (24 >= hoursSince && hoursSince <= 48) contractValueYesterday += c.TotalCost;
                if (hoursSince <= 168) contractValueWeek += c.TotalCost;
                if (hoursSince <= 720) contractValueMonth += c.TotalCost; // Assumption: Month = 30 days
                if (hoursSince <= 365) contractValueYear += c.TotalCost;  // Assumption: Year = 365 days
            }
                        
            Console.Clear();
            Console.WriteLine($"The Database contains {totalItems} objects.");
            Console.WriteLine($"\n{contracts.Count} are present, with a total value of {totalContractValue}.");
            Console.WriteLine($"\t         Today: {contractValueToday} CHF");
            Console.WriteLine($"\t     Yesterday: {contractValueYesterday} CHF");
            Console.WriteLine($"\t   Last 7 days: {contractValueWeek} CHF");
            Console.WriteLine($"\t  Last 30 days: {contractValueMonth} CHF");
            Console.WriteLine($"\t Last 365 days: {contractValueYear} CHF");
        }
        
        private void List() {
            // Print info of every element
            foreach (Manufacturer element in manufacturers) { element.PrintInfo(); }
            foreach (Car element in cars) { element.PrintInfo(); }
            foreach (Van element in vans) { element.PrintInfo(); }
            foreach (Bus element in buses) { element.PrintInfo(); }
            foreach (Truck element in trucks) { element.PrintInfo(); }
            // foreach (Contract element in contracts) { element.PrintInfo(); }
            foreach (Customer element in customers) { element.PrintInfo(); }
            foreach (Employee element in employees) { element.PrintInfo(); }
            // foreach (Equipment element in equipment) { element.PrintInfo(); }
            foreach (License element in licenses) { element.PrintInfo(); }
            foreach (Plate element in plates) { element.PrintInfo(); }
        }

        private void CreateNew()
        {
            while (true)
            {
                // create Object

                #region menu

                Console.WriteLine("\nWhat object do you want to create?");
                Console.WriteLine("Ca Car");
                Console.WriteLine("V  Van");
                Console.WriteLine("B  Bus");
                Console.WriteLine("T  Truck\n");

                Console.WriteLine("M  Manufacturer");
                //Console.WriteLine("Co Contract");
                Console.WriteLine("Cu Customer");
                Console.WriteLine("Em Employee");
                Console.WriteLine("Eq Equipment");
                Console.WriteLine("L  License");
                Console.WriteLine("P  Plate\n");

                Console.WriteLine("Q  Go Back");

                #endregion

                switch (Console.ReadLine().ToUpper())
                {
                    case "CA":
                        Console.Clear();
                        Console.Write("\nCreating car, please enter required information");
                        cars.Add(new Car(selectManufacturer(), getText("model", 50), selectPricePerDay(),
                            selectDoorCount(), selectXWheelCount(),
                            selectSeatCount(), selectIfHas("self-driving functionality"),
                            selectIfHas("a trailer-hitch"), selectPlate()));
                        Console.WriteLine("\nYour new car has been added :)");
                        break;
                    case "V":
                        Console.Clear();
                        Console.Write("\nCreating van, please enter information");
                        vans.Add(new Van(selectManufacturer(), getText("model", 50), selectPricePerDay(),
                            selectDoorCount(), selectXWheelCount(),
                            selectMaxWeight(), selectIfHas("self-driving functionality"),
                            selectIfHas("a trailer-hitch"), selectPlate(), selectIfHas("a side door")));
                        Console.WriteLine("\nYour new van has been added :)");
                        break;
                    case "B":
                        Console.Clear();
                        Console.Write("\nCreating bus, please enter information");
                        buses.Add(new Bus(selectManufacturer(), getText("model", 50), selectPricePerDay(),
                            selectDoorCount(), selectXWheelCount(),
                            selectSeatCount(), selectAxisCount(), selectIfHas("self-driving functionality"),
                            selectIfHas("a trailer-hitch"), selectPlate(), selectIfHas("two floors")));
                        Console.WriteLine("\nYour new bus has been added :)");
                        break;
                    case "T":
                        Console.Clear();
                        Console.Write("\nCreating truck, please enter information");
                        trucks.Add(new Truck(selectManufacturer(), getText("model", 50), selectPricePerDay(),
                            selectDoorCount(), selectXWheelCount(), selectMaxWeight(), selectAxisCount(),
                            selectIfHas("self-driving functionality"), selectIfHas("a trailer-hitch"), selectPlate()));
                        Console.WriteLine("\nYour new truck has been added :)");
                        break;

                    case "M":
                        Console.Clear();
                        Console.Write("\nCreating manufacturer, please enter information");
                        manufacturers.Add(new Manufacturer(getText("manufacturer", 60)));
                        Console.WriteLine("\nYour new manufacturer has been added :)");
                        break;
                    case "CU":
                        Console.Clear();
                        Console.Write("\nCreating customer, please enter information");
                        customers.Add(new Customer(getText("customer", 45), selectLanguage()));
                        Console.WriteLine("\nYour new customer has been added :)");
                        break;
                    case "EM":
                        Console.Clear();
                        Console.Write("\nCreating customer, please enter information");
                        employees.Add(new Employee(getText("customer", 45), selectLanguage(), selectWorkerType()));
                        Console.WriteLine("\nYour new customer has been added :)");
                        break;
                    case "EQ":
                        Console.Clear();
                        Console.Write("\nCreating equipment, please enter information");
                        // equipment.Add(new Equipment(getText("equipment", 50),
                        //     getText("equipment description", maxLength: 500, true)));
                        Console.WriteLine("\nYour new equipment has been added :)");
                        break;
                    case "LI":
                        Console.Clear();
                        Console.Write("\nCreating a new license, please enter information");
                        licenses.Add(new License(selectCustomer()));
                        Console.WriteLine("\nYour new license has been added :)");
                        break;

                    case "Q":
                        return;

                    default:
                        Console.Clear();
                        Console.WriteLine("Your selection is not valid, please try again!");
                        break;
                }
            }
        }

        private void DeleteObject()
        {
            while (true)
            {
                // create Object

                #region menu

                Console.WriteLine("\nWhat object do you want to delete?");
                Console.WriteLine("Ca Car");
                Console.WriteLine("V  Van");
                Console.WriteLine("B  Bus");
                Console.WriteLine("T  Truck\n");
                
                Console.WriteLine("M  Manufacturer");
                //Console.WriteLine("Co Contract");
                Console.WriteLine("Cu Customer");
                Console.WriteLine("Em Employee");
                Console.WriteLine("Eq Equipment");
                Console.WriteLine("L  License");
                Console.WriteLine("P  Plate\n");

                Console.WriteLine("Q  Go Back");

                #endregion

                switch (Console.ReadLine().ToUpper())
                {
                    case "CA":
                        DeleteFromList(cars);
                        break;
                    case "V":
                        Console.WriteLine("\nPlease select the van you want to delete:");
                        vans.Remove(vans[selectIdFromList(vans)]);
                        Console.WriteLine("\nYour van has been successfully deleted :)");
                        break;
                    case "B":
                        Console.Clear();
                        Console.Write("\nCreating bus, please enter information");
                        buses.Add(new Bus(selectManufacturer(), getText("model", 50), selectPricePerDay(),
                            selectDoorCount(), selectXWheelCount(),
                            selectSeatCount(), selectAxisCount(), selectIfHas("self-driving functionality"),
                            selectIfHas("a trailer-hitch"), selectPlate(), selectIfHas("two floors")));
                        Console.WriteLine("\nYour new bus has been added :)");
                        break;
                    case "T":
                        Console.Clear();
                        Console.Write("\nCreating truck, please enter information");
                        trucks.Add(new Truck(selectManufacturer(), getText("model", 50), selectPricePerDay(),
                            selectDoorCount(), selectXWheelCount(), selectMaxWeight(), selectAxisCount(),
                            selectIfHas("self-driving functionality"), selectIfHas("a trailer-hitch"), selectPlate()));
                        Console.WriteLine("\nYour new truck has been added :)");
                        break;

                    case "M":
                        Console.Clear();
                        Console.Write("\nCreating manufacturer, please enter information");
                        manufacturers.Add(new Manufacturer(getText("manufacturer", 60)));
                        Console.WriteLine("\nYour new manufacturer has been added :)");
                        break;
                    case "CU":
                        Console.Clear();
                        Console.Write("\nCreating customer, please enter information");
                        customers.Add(new Customer(getText("customer", 45), selectLanguage()));
                        Console.WriteLine("\nYour new customer has been added :)");
                        break;
                    case "EM":
                        Console.Clear();
                        Console.Write("\nCreating customer, please enter information");
                        employees.Add(new Employee(getText("customer", 45), selectLanguage(), selectWorkerType()));
                        Console.WriteLine("\nYour new customer has been added :)");
                        break;
                    case "EQ":
                        Console.Clear();
                        Console.Write("\nCreating equipment, please enter information");
                        // equipment.Add(new Equipment(getText("equipment", 50),
                        //     getText("equipment description", maxLength: 500, true)));
                        Console.WriteLine("\nYour new equipment has been added :)");
                        break;
                    case "LI":
                        Console.Clear();
                        Console.Write("\nCreating a new license, please enter information");
                        licenses.Add(new License(selectCustomer()));
                        Console.WriteLine("\nYour new license has been added :)");
                        break;

                    case "Q":
                        return;

                    default:
                        Console.WriteLine("Your selection is not valid, please try again!");
                        break;
                }
            }
        }

        private Manufacturer selectManufacturer()
        {
            while (true)
            {
                Console.WriteLine("\nEnter Manufacturer:");
                for (int i = 0; i < manufacturers.Count; i++) 
                    Console.WriteLine($"{i} {manufacturers[i].Name}");
            
                // read input and check if it is a number
                string rawInput = Console.ReadLine();
                if (!int.TryParse(rawInput, out _))
                    Console.WriteLine("Your input is not valid, please try again!");
                else
                {
                    // Check if the manufacturer exists
                    int manufacturerIndex = Convert.ToInt32(rawInput);
                    if (manufacturers.Count < manufacturerIndex)
                        Console.WriteLine("The selected Manufacturer does NOT exist, please try again!");
                    else return manufacturers[manufacturerIndex];
                }
            }
        }
        private int selectMaxWeight()
        {
            while (true)
            {
                Console.WriteLine("\nEnter Max supported weight:");
                
                // read input and check if it is a number
                string rawInput = Console.ReadLine();
                if (!int.TryParse(rawInput, out _))
                    Console.WriteLine("Your input is not valid, please enter a number!");
                else
                {
                    return Convert.ToInt32(rawInput);
                }
            }
        }
        private int selectAxisCount()
        {
            while (true)
            {
                Console.WriteLine("\nEnter the count of axis in the vehicle:");
                
                // read input and check if it is a number
                string rawInput = Console.ReadLine();
                if (!int.TryParse(rawInput, out _))
                    Console.WriteLine("Your input is not valid, please enter a number!");
                else
                    return Convert.ToInt32(rawInput);
            }
        }
        private Plate selectPlate()
        {
            while (true)
            {
                Console.WriteLine("\nEnter Plate Number:");
                foreach (Plate p in plates)
                    Console.WriteLine($"{p.Number}");

                string rawInput = Console.ReadLine();
                // Check if plate exists
                foreach (Plate p in plates)
                    if (p.Number == rawInput)
                        return p;
                Console.WriteLine("This plate could not be found, please try again!");
            }
        }
        private Language selectLanguage()
        {
            while (true)
            {
                Console.WriteLine("\nWhat language does he/she/they speak/prefer:");
                foreach (Language l in Enum.GetValues(typeof(Language)))
                    Console.WriteLine($"{l.ToString()}");

                string rawInput = Console.ReadLine();
                foreach (Language l in Enum.GetValues(typeof(Language)))
                    if (l.ToString() == rawInput)
                        return l;
                Console.WriteLine("This language could not be found, please try again!");
            }
        }
        private Employee selectEmployee()
        {
            while (true)
            {
                Console.WriteLine("\nSelect Employee:");
                for (var i = 0; i < employees.Count; i++)
                    Console.WriteLine($"{i} {employees[i].Name}");

                string rawInput = Console.ReadLine();
                if (!int.TryParse(rawInput, out _))
                    Console.WriteLine("Your input is not valid, please enter a number (the number next to the name of the employee)!");
                else
                {
                    if (employees.Count < Convert.ToInt32(rawInput))
                        Console.WriteLine("This employee does not exist, please try again!");
                    else
                        return employees[Convert.ToInt32(rawInput)];
                }
            }
        }
        private Customer selectCustomer()
        {
            while (true)
            {
                Console.WriteLine("\nWhat customer does this license belong to?");
                for (var i = 0; i < customers.Count; i++)
                    Console.WriteLine($"{i} {customers[i].Name}");

                string rawInput = Console.ReadLine();
                if (!int.TryParse(rawInput, out _))
                    Console.WriteLine("Your input is not valid, please enter a number (the number next to the name of the customer)!");
                else
                {
                    return customers[Convert.ToInt32(rawInput)];
                }
                Console.WriteLine("This customer does not exist, please try again!");
            }
        }
        private float selectPricePerDay()
        {
            while (true)
            {
                Console.WriteLine("\nWhat does the vehicle cost per day?");

                string rawInput = Console.ReadLine();
                if (!int.TryParse(rawInput, out _))
                    Console.WriteLine("Your input is not valid, please enter a number!");
                else
                {
                    return Convert.ToInt32(rawInput);
                }
            }
        }
        private WorkerType selectWorkerType()
        {
            while (true)
            {
                Console.WriteLine("\nWhat type of worker is this employee:");
                foreach (WorkerType w in Enum.GetValues(typeof(WorkerType)))
                    Console.WriteLine($"{w.ToString()}");

                string rawInput = Console.ReadLine();
                foreach (WorkerType w in Enum.GetValues(typeof(WorkerType)))
                    if (w.ToString() == rawInput)
                        return w;
                Console.WriteLine("This language could not be found, please try again!");
            }
        }
        private string getText(string whatFor, int maxLength = 500, bool hideNameText = false)
        {
            while (true)
            {
                Console.WriteLine($"\nEnter {whatFor} {((hideNameText) ? "" : "name")}:");

                string rawInput = Console.ReadLine();
                if (rawInput.Length > maxLength)
                    Console.WriteLine($"The name of the {whatFor} can't be longer than {maxLength} digits");
                else
                    return rawInput;
            }
        }
        private int selectDoorCount()
        {
            while (true)
            {
                Console.WriteLine("How many doors does the vehicle have?");

                string rawInput = Console.ReadLine();
                // Check if is number
                if (int.TryParse(rawInput, out _))
                    return Convert.ToInt32(rawInput);
                Console.WriteLine("You did not enter a valid number, please try again!");
            }
        }
        private int selectXWheelCount()
        {
            while (true)
            {
                Console.WriteLine("How many wheels are motorized?");

                string rawInput = Console.ReadLine();
                // Check if is number
                if (int.TryParse(rawInput, out _))
                    return Convert.ToInt32(rawInput);
                Console.WriteLine("You did not enter a valid number, please try again!");
            }
        }
        private int selectSeatCount()
        {
            while (true)
            {
                Console.WriteLine("How many seats does the vehicle have?");

                string rawInput = Console.ReadLine();
                // Check if is number
                if (int.TryParse(rawInput, out _))
                    return Convert.ToInt32(rawInput);
                Console.WriteLine("You did not enter a valid number, please try again!");
            }
        }
        private bool selectIfHas(string what)
        {
            return YesOrNoQuestion($"\nDoes the vehicle have {what}? ");
        }

        public static bool YesOrNoQuestion(string question)
        {
            while (true)
            {
                Console.Write(question + " ");
                switch (Console.ReadLine().ToLower())
                {
                    case "yes":
                        return true;
                    case "no":
                        return false;
                    default:
                        Console.WriteLine("Your input was invalid, please enter 'yes' or 'no'!");
                        break;
                }
            }
            
        }
        private void SaveAndExit()
        {
            Console.WriteLine("Saving changes to disk...");
                        
            // Write Data to Json Files
            Storage.WriteJson(filepath + "cars.json", cars);
            Storage.WriteJson(filepath + "vans.json", vans);
            Storage.WriteJson(filepath + "buses.json", buses);
            Storage.WriteJson(filepath + "trucks.json", trucks);
            Storage.WriteJson(filepath + "manufacturers.json", manufacturers);
            Storage.WriteJson(filepath + "contracts.json", contracts);
            Storage.WriteJson(filepath + "customers.json", customers);
            Storage.WriteJson(filepath + "employees.json", employees);
            // Storage.WriteJson(filepath + "equipment.json", equipment);
            Storage.WriteJson(filepath + "licenses.json", licenses);
            Storage.WriteJson(filepath + "plates.json", plates);
                    
            Console.WriteLine("Done saving changes, goodbye!");
            System.Environment.Exit(1);
        }

        private void QuitWithoutSaving()
        {
            // Ask user if really wants to quit, because could loose change
            if (YesOrNoQuestion("Do you really want to quit?"))
                System.Environment.Exit(1);
        }

        private void DeleteFromList<T>(List<T> list)
        {
            Console.WriteLine($"\nPlease select the {list[0].GetType().Name} you want to delete:");
            list.Remove(list[selectIdFromList(list)]);
            Console.WriteLine($"\nYour {list[0].GetType().Name} has been successfully deleted :)");
        }
        
        private int selectIdFromList<T>(List<T> type)
        {
            while (true)
            {
                Console.WriteLine("\nSelect:");
                int i = 0; // (only using foreach because for resulted in bugs)
                foreach (dynamic v in type)
                { 
                    Console.WriteLine($"{i} {v.Manufacturer.Name} {v.Model} ({v.PricePerDay} CHF per day)");
                    i++;
                }
                
                string rawInput = Console.ReadLine();
                if (!int.TryParse(rawInput, out _))
                    Console.WriteLine("Your input is not valid, please enter a number (the number next to the name)!");
                else
                {
                    if (type.Count < Convert.ToInt32(rawInput))
                        Console.WriteLine("This employee does not exist, please try again!");
                    else
                        return Convert.ToInt32(rawInput);
                }
            }
        }
    }
}