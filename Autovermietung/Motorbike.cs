﻿namespace Autovermietung
{
    public class Motorbike : Vehicle
    {
        public int Axis { get; }
        
        public Motorbike(Manufacturer manufacturer, string model, float pricePerDay, int doors, int xWheelDrive,
            bool isSelfDriving, bool hasTrailerHitch = false, Plate plate = null, int axis = 2) :
            base(manufacturer, model, pricePerDay, doors, xWheelDrive, isSelfDriving, hasTrailerHitch, plate)
        {
            Axis = axis;
        }
    }
}