﻿using System;
using System.Collections.Generic;
using Autovermietung.Exception;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Autovermietung.Test
{
    [TestClass]
    public class ContractUnitTest
    {
        [TestMethod]
        public void TestIsValid_Valid()
        {
            // arrange
            DateTime date = DateTime.Today;
            Customer customer = new Customer("Max Mustermann", Language.De);
            License license = new License(customer);
            customer.License = license;
            Employee employee = new Employee("Maxi Musti", Language.De);
            Vehicle vehicle = new Bus(new Manufacturer("Bus300"), "X4000", 4.50, 2, 4,  48);
            List<Vehicle> vehicles = new List<Vehicle>();
            vehicles.Add(vehicle);
            
            // act
            Contract contract = new Contract(customer, employee, vehicles, date, date);
            
            // assert
            Assert.IsTrue(contract.IsValid());
        }
        
        [TestMethod]
        public void TestIsValid_Invalid()
        {
            // arrange
            DateTime date = new DateTime(2020, 11, 10);
            Customer customer = new Customer("Max Mustermann", Language.De);
            License license = new License(customer);
            customer.License = license;
            Employee employee = new Employee("Maxi Musti", Language.De);
            Vehicle vehicle = new Bus(new Manufacturer("Bus300"), "X4000", 4.50, 2, 4,  48);
            List<Vehicle> vehicles = new List<Vehicle>();
            vehicles.Add(vehicle);
            
            
            // act
            Contract contract = new Contract(customer, employee, vehicles, date, date);
            
            // assert
            Assert.IsFalse(contract.IsValid());
        }

        [TestMethod]
        public void TestInvalidLicense()
        {
            // arrange
            DateTime date = new DateTime(2020, 11, 10);
            Customer customer = new Customer("Max Mustermann", Language.De);
            License license = new License(customer, false);
            customer.License = license;
            Employee employee = new Employee("Maxi Musti", Language.De);
            
            // act => assert
            Assert.ThrowsException<InvalidLicenseException>(() =>
                new Contract(customer, employee, new List<Vehicle>(), date, date));
        }
        
        [TestMethod]
        public void TestNoLicense()
        {
            // arrange
            DateTime date = new DateTime(2020, 11, 10);
            Customer customer = new Customer("Max Mustermann", Language.De);
            Employee employee = new Employee("Maxi Musti", Language.De);
            
            // act

            // act => assert
            Assert.ThrowsException<InvalidLicenseException>(() =>
                new Contract(customer, employee, new List<Vehicle>(), date, date));
        }
    }
}